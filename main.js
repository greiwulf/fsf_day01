//load libraries
var express = require("express");
var path = require("path");

// create an instance of express application
var app = express();

// define source
app.use(express.static(__dirname + "/public"));
console.info(">> __dirname" + __dirname);

app.use(function(rep,resp){
    resp.status('404');
    resp.type("text/html");
    resp.send("<h1>File not found!</h1>");
});

//setting the port as property of the app
app.set("port", 3000);

console.info("Port: " + app.get("port"));

// start the server onn the specified port
app.listen(app.get("port"), function() {
    console.info("Application is listening on port " + app.get("port"));
    console.info("Yaaaaahhh...");
});